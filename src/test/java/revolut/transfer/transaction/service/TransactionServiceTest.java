package revolut.transfer.transaction.service;

import org.jdbi.v3.core.JdbiException;
import org.jdbi.v3.sqlobject.transaction.Transactional;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import revolut.transfer.transaction.db.AccountDAO;
import revolut.transfer.transaction.db.TransactionDAO;
import revolut.transfer.transaction.entry.Account;
import revolut.transfer.transaction.entry.Transaction;
import revolut.transfer.transaction.entry.TransactionRequest;
import revolut.transfer.util.TimestampUtil;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAcceptableException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

class TransactionServiceTest {

    private final TransactionDAO transactionDAO = mock(TransactionDAO.class);
    private final AccountDAO accountDAO = mock(AccountDAO.class);

    private TransactionService transactionService;

    private static Random random = new Random();

    @BeforeEach
    void setUp() {
        transactionService = new TransactionService(accountDAO, transactionDAO);
    }

    @AfterEach
    void tearDown() {
        reset(transactionDAO);
        reset(accountDAO);
    }

    @Test
    void testGetTransaction_ok() {
        var expectedTransaction = Transaction.builder()
                .id(1L)
                .amount(BigDecimal.valueOf(random.nextDouble()))
                .senderId(random.nextLong())
                .receiverId(random.nextLong())
                .build();

        when(transactionDAO.findById(1L))
                .thenReturn(Optional.of(expectedTransaction));

        assertEquals(expectedTransaction, transactionService.fetchById(1L));
    }

    @Test
    void testGetTransaction_error() {
        when(transactionDAO.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(BadRequestException.class, () ->
            transactionService.fetchById(1L));
    }

    @Test
    void testGetTransactionsByAccountId_ok() {
        var t1 = Transaction.builder()
                .id(random.nextLong())
                .amount(BigDecimal.valueOf(random.nextDouble()))
                .senderId(1L)
                .receiverId(2L)
                .build();

        var t2 = Transaction.builder()
                .id(random.nextLong())
                .amount(BigDecimal.valueOf(random.nextDouble()))
                .senderId(2L)
                .receiverId(1L)
                .build();

        var expectedList = List.of(t1, t2);
        when(transactionDAO.findByAccountId(1L, 10, 0))
                .thenReturn(expectedList);

        assertEquals(expectedList, transactionService.findByAccountId(1L, 0));
    }

    @Test
    void testGetTransactionsByAccountId_pagination() {
        var transactionList = new LinkedList<Transaction>();

        for (long i = 0; i < 100; i++) {
            var t = Transaction.builder()
                    .id(i)
                    .amount(BigDecimal.valueOf(random.nextDouble()))
                    .senderId(1L)
                    .receiverId(2L)
                    .timestamp(TimestampUtil.now())
                    .build();
            transactionList.add(t);
        }

        var pageNumber = 2;
        var offset = pageNumber * TransactionService.PAGE_SIZE;
        var expectedList = transactionList.stream()
                .skip(offset)
                .limit(TransactionService.PAGE_SIZE)
                .collect(Collectors.toList());

        when(transactionDAO.findByAccountId(1L, TransactionService.PAGE_SIZE, offset))
                .thenReturn(expectedList);

        System.out.println(expectedList);

        var actualTransactions = transactionService.findByAccountId(1L, pageNumber);
        System.out.println(actualTransactions);
        assertEquals(expectedList, actualTransactions);
    }

    @Test
    void testGetTransactionsByAccountId_empty() {
        when(transactionDAO.findByAccountId(1L, TransactionService.PAGE_SIZE, 0))
                .thenReturn(List.of());

        assertEquals(List.of(), transactionService.findByAccountId(1L, 0));
    }

    @Test
    void testUpdateAccount_ok() {
        when(accountDAO.updateBalance(1L, BigDecimal.valueOf(random.nextDouble())))
                .thenReturn(1L);

        transactionService.updateAccount(1L, BigDecimal.valueOf(random.nextDouble()));
    }

    @Test
    void testUpdateAccount_error() {
        when(accountDAO.updateBalance(1L, BigDecimal.valueOf(1.1)))
                .thenThrow(new JdbiException("") {
                    @Override
                    public String getMessage() {
                        return super.getMessage();
                    }
                });

        assertThrows(InternalServerErrorException.class, () ->
                transactionService.updateAccount(1L, BigDecimal.valueOf(1.1)));
    }

    @Test
    void testChangeBalanceForSenderAndReceiver_ok() {
        when(accountDAO.findById(1L)).thenReturn(Optional.of(new Account(1L, BigDecimal.valueOf(100))));
        when(accountDAO.findById(2L)).thenReturn(Optional.of(new Account(2L, BigDecimal.valueOf(100))));

        var transactionRequest = new TransactionRequest(BigDecimal.valueOf(10.99), 1L, 2L);
        transactionService.changeBalanceForSenderAndReceiver(transactionRequest);
    }

    @Test
    void testChangeBalanceForSenderAndReceiver_error_noSender() {
        when(accountDAO.findById(1L)).thenReturn(Optional.empty());
        when(accountDAO.findById(2L)).thenReturn(Optional.of(new Account(2L, BigDecimal.valueOf(100))));

        var transactionRequest = new TransactionRequest(BigDecimal.valueOf(10), 1L, 2L);
        assertThrows(BadRequestException.class, () ->
                transactionService.changeBalanceForSenderAndReceiver(transactionRequest));
    }

    @Test
    void testChangeBalanceForSenderAndReceiver_error_noReceiver() {
        when(accountDAO.findById(1L)).thenReturn(Optional.of(new Account(1L, BigDecimal.valueOf(100))));
        when(accountDAO.findById(2L)).thenReturn(Optional.empty());

        var transactionRequest = new TransactionRequest(BigDecimal.valueOf(10), 1L, 2L);
        assertThrows(BadRequestException.class, () ->
                transactionService.changeBalanceForSenderAndReceiver(transactionRequest));    }

    @Test
    void testChangeBalanceForSenderAndReceiver_error_balanceNotEnough() {
        when(accountDAO.findById(1L)).thenReturn(Optional.of(new Account(1L, BigDecimal.valueOf(1))));
        when(accountDAO.findById(2L)).thenReturn(Optional.of(new Account(2L, BigDecimal.valueOf(100))));

        var transactionRequest = new TransactionRequest(BigDecimal.valueOf(10), 1L, 2L);
        assertThrows(NotAcceptableException.class, () ->
                transactionService.changeBalanceForSenderAndReceiver(transactionRequest));    }

    @Test
    @SuppressWarnings("unchecked")
    void testInsertTransaction_ok() {
        when(transactionDAO.insert(any(BigDecimal.class), anyLong(), anyLong(), any(Timestamp.class)))
                .thenReturn(1L);

        final Transactional<AccountDAO> accountTx = mock(Transactional.class);

        transactionService.insertTransactionWithoutTransactional
                (new TransactionRequest(BigDecimal.valueOf(random.nextDouble()), 1L, 2L), accountTx);

        verify(transactionDAO, times(1))
                .insert(any(BigDecimal.class), anyLong(), anyLong(), any(Timestamp.class));
        verify(accountTx, never()).rollback();
    }

    @Test
    @SuppressWarnings("unchecked")
    void testInsertTransaction_error() {
        when(transactionDAO.insert(any(BigDecimal.class), anyLong(), anyLong(), any(Timestamp.class)))
                .thenThrow(new JdbiException("") {
                    @Override
                    public String getMessage() {
                        return super.getMessage();
                    }
                });

        final Transactional<AccountDAO> accountTx = mock(Transactional.class);

        assertThrows(InternalServerErrorException.class, () ->
                transactionService.insertTransactionWithoutTransactional(
                        new TransactionRequest(BigDecimal.valueOf(random.nextDouble()), 1L, 2L), accountTx)
        );
        verify(transactionDAO, times(1))
                .insert(any(BigDecimal.class), anyLong(), anyLong(), any(Timestamp.class));
        verify(accountTx, times(1)).rollback();
    }
}