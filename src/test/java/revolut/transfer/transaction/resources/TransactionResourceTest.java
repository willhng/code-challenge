package revolut.transfer.transaction.resources;

import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.testing.ConfigOverride;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.eclipse.jetty.http.HttpStatus;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import revolut.transfer.TransferApplication;
import revolut.transfer.TransferConfiguration;
import revolut.transfer.transaction.db.AccountDAO;
import revolut.transfer.transaction.db.TransactionDAO;
import revolut.transfer.transaction.entry.Transaction;
import revolut.transfer.transaction.entry.TransactionRequest;
import revolut.transfer.transaction.service.TransactionService;
import revolut.transfer.util.TimestampUtil;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@ExtendWith(DropwizardExtensionsSupport.class)
class TransactionResourceTest {

    private static final DropwizardAppExtension<TransferConfiguration> RULE = new DropwizardAppExtension<>(
            TransferApplication.class, ResourceHelpers.resourceFilePath("config.yml"),
            ConfigOverride.config("database.url", "jdbc:h2:mem:testdb"));

    private static WebTarget targetedClient;

    private static AccountDAO accountDAO;
    private static TransactionDAO transactionDAO;
    private static long senderId;
    private static long receiverId;

    private static Random random = new Random();

    @BeforeAll
    static void beforeAll() {
        final Jdbi jdbi = new JdbiFactory()
                .build(RULE.getEnvironment(), RULE.getConfiguration().getDatabaseSourceFactory(), "test");
        accountDAO = jdbi.onDemand(AccountDAO.class);
        transactionDAO = jdbi.onDemand(TransactionDAO.class);

        targetedClient = RULE.client()
                .target(String.format("http://localhost:%d/transactions", RULE.getLocalPort()));

        senderId = accountDAO.insert(BigDecimal.valueOf(100));
        receiverId = accountDAO.insert(BigDecimal.valueOf(100));
    }

    @AfterEach
    void afterEach() {
        transactionDAO.deleteAll();
    }

    @Test
    void testCreateTransaction_ok() {
        var transferAmount = BigDecimal.ONE;
        var transactionRequest = new TransactionRequest(transferAmount, senderId, receiverId);
        var response = targetedClient
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(transactionRequest, MediaType.APPLICATION_JSON));

        assertEquals(HttpStatus.OK_200, response.getStatus());

        var actualTransaction = response.readEntity(Transaction.class);
        assertEquals(senderId, actualTransaction.getSenderId());
        assertEquals(receiverId, actualTransaction.getReceiverId());
        assertEquals(transferAmount, actualTransaction.getAmount());
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/transfer-amount-calculation.csv", numLinesToSkip = 1)
    void testCreateTransaction_ok_amountCalculation(
            final BigDecimal senderOldBalance,
            final BigDecimal receiverOldBalance,
            final BigDecimal transferAmount,
            final BigDecimal senderNewBalance,
            final BigDecimal receiverNewBalance
    ) {
        accountDAO.updateBalance(senderId, senderOldBalance);
        accountDAO.updateBalance(receiverId, receiverOldBalance);
        assertEquals(senderOldBalance, accountDAO.findById(senderId).get().getBalance());
        assertEquals(receiverOldBalance, accountDAO.findById(receiverId).get().getBalance());

        var transactionRequest = new TransactionRequest(transferAmount, senderId, receiverId);
        var response = targetedClient
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(transactionRequest, MediaType.APPLICATION_JSON));

        assertEquals(HttpStatus.OK_200, response.getStatus());

        var actualTransaction = response.readEntity(Transaction.class);
        assertEquals(senderId, actualTransaction.getSenderId());
        assertEquals(receiverId, actualTransaction.getReceiverId());
        assertEquals(transferAmount, actualTransaction.getAmount());
        assertEquals(senderNewBalance, accountDAO.findById(senderId).get().getBalance());
        assertEquals(receiverNewBalance, accountDAO.findById(receiverId).get().getBalance());
    }

    @Test
    void testCreateTransaction_error_noSender() {
        var transactionRequest = new TransactionRequest(BigDecimal.ONE, random.nextLong(), receiverId);
        var response = targetedClient
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(transactionRequest, MediaType.APPLICATION_JSON));
        assertEquals(HttpStatus.BAD_REQUEST_400, response.getStatus());
    }

    @Test
    void testCreateTransaction_error_noReceiver() {
        var transactionRequest = new TransactionRequest(BigDecimal.ONE, senderId, random.nextLong());
        var response = targetedClient
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(transactionRequest, MediaType.APPLICATION_JSON));
        assertEquals(HttpStatus.BAD_REQUEST_400, response.getStatus());
    }

    @Test
    void testCreateTransaction_error_invalidBalance() {
        var transactionRequest = new TransactionRequest(BigDecimal.valueOf(-1), senderId, receiverId);
        var response = targetedClient
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(transactionRequest, MediaType.APPLICATION_JSON));
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY_422, response.getStatus());
    }

    @Test
    void testCreateTransaction_error_balanceNotEnough() {
        var transactionRequest = new TransactionRequest(BigDecimal.valueOf(1000), senderId, receiverId);
        var response = targetedClient
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(transactionRequest, MediaType.APPLICATION_JSON));
        assertEquals(HttpStatus.NOT_ACCEPTABLE_406, response.getStatus());
    }

    @Test
    void testGetTransaction_ok() {
        var expectedTransaction = Transaction.builder()
                .amount(BigDecimal.valueOf(random.nextDouble()))
                .senderId(random.nextLong())
                .receiverId(random.nextLong())
                .timestamp(TimestampUtil.now())
                .build();

        final long id = transactionDAO.insert(
                expectedTransaction.getAmount(),
                expectedTransaction.getSenderId(),
                expectedTransaction.getReceiverId(),
                expectedTransaction.getTimestamp()
        );

        final Transaction actualTransaction = targetedClient
                .path("/" + id)
                .request(MediaType.APPLICATION_JSON)
                .get(Transaction.class);

        expectedTransaction.setId(id);

        assertEquals(expectedTransaction, actualTransaction);
    }

    @Test
    void testGetTransaction_badRequest() {
        var response = targetedClient
                .path("/" + random.nextLong())
                .request(MediaType.APPLICATION_JSON)
                .get();
        assertEquals(HttpStatus.BAD_REQUEST_400, response.getStatus());
    }

    @Test
    void testGetTransaction_wrongIdFormat() {
        var response = targetedClient
                .path("/a")
                .request(MediaType.APPLICATION_JSON)
                .get();
        assertEquals(HttpStatus.NOT_FOUND_404, response.getStatus());
    }

    @Test
    void testFindByAccountId_ok() {
        var transactions = new LinkedList<Transaction>();
        for (long i = 0; i < 15; i++) {
            var t = Transaction.builder()
                    .amount(BigDecimal.valueOf(i))
                    .senderId(1L)
                    .receiverId(2L)
                    .timestamp(TimestampUtil.now())
                    .build();
            transactions.add(t);
            transactionDAO.insert(t.getAmount(), t.getSenderId(), t.getReceiverId(), t.getTimestamp());
        }
        Collections.reverse(transactions);

        var expectedTransactionsPage0 = transactions.stream()
                .limit(TransactionService.PAGE_SIZE)
                .collect(Collectors.toList());

        List<Transaction> actualTransactionsPage0 = targetedClient
                .path("/for/account/1")
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<>() {
                });

        // Fulfill the expected transactions' id
        for (int i = 0; i < expectedTransactionsPage0.size(); i++) {
            expectedTransactionsPage0.get(i).setId(actualTransactionsPage0.get(i).getId());
        }

        assertEquals(expectedTransactionsPage0, actualTransactionsPage0);
    }

    @Test
    void testFindByAccountId_ok_page1() {
        var transactions = new LinkedList<Transaction>();
        for (long i = 0; i < 15; i++) {
            var t = Transaction.builder()
                    .amount(BigDecimal.valueOf(i))
                    .senderId(1L)
                    .receiverId(2L)
                    .timestamp(TimestampUtil.now())
                    .build();
            transactions.add(t);
            transactionDAO.insert(t.getAmount(), t.getSenderId(), t.getReceiverId(), t.getTimestamp());
        }
        Collections.reverse(transactions);

        // check the 2nd page
        var pageNumber = 1;
        var offset = pageNumber * TransactionService.PAGE_SIZE;
        var expectedTransactionsPage1 = transactions.stream()
                .skip(offset)
                .limit(TransactionService.PAGE_SIZE)
                .collect(Collectors.toList());

        List<Transaction> actualTransactionsPage1 = targetedClient
                .path("/for/account/1")
                .queryParam("page", pageNumber)
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<>() {
                });

        // Fulfill the expected transactions' id
        for (int i = 0; i < expectedTransactionsPage1.size(); i++) {
            expectedTransactionsPage1.get(i).setId(actualTransactionsPage1.get(i).getId());
        }

        assertEquals(expectedTransactionsPage1, actualTransactionsPage1);
    }

    @Test
    void testFindByAccountId_empty() {
        final Response response = targetedClient
                .path("/for/account/1")
                .queryParam("page", 100)
                .request(MediaType.APPLICATION_JSON)
                .get();
        assertEquals(HttpStatus.OK_200, response.getStatus());
        assertEquals(0, response.readEntity(new GenericType<List<Transaction>>() {
        }).size());
    }
}