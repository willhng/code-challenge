package revolut.transfer.transaction.resources;

import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import revolut.transfer.transaction.entry.Transaction;
import revolut.transfer.transaction.entry.TransactionRequest;
import revolut.transfer.transaction.service.TransactionService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Api
@AllArgsConstructor
@Path("/transactions")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionResource {

    private final TransactionService transactionService;

    @ApiOperation("Get a transaction by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "No such transaction id"),
    })
    @GET
    @Path("/{id}")
    @Timed
    public Transaction getTransaction(@PathParam("id") final long id)
            throws ClientErrorException {

        return transactionService.fetchById(id);
    }

    @ApiOperation("Find paged transactions by account id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
    })
    @GET
    @Path("/for/account/{accountId}")
    @Timed
    public List<Transaction> findByAccountId(
            @PathParam("accountId") final long id,
            @QueryParam("page") final int page
    ) throws ClientErrorException {

        return transactionService.findByAccountId(id, page);
    }

    @ApiOperation(
            value = "Create the transaction according to the transfer.",
            notes = "This operation is transactional.\n" +
                    "For you easy to create transaction, there are 2 accounts are prepared for you, the IDs are 1 and 2."
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "No such account id"),
            @ApiResponse(code = 406, message = "Sender's balance is not enough"),
            @ApiResponse(code = 500, message = "Unexpected internal transaction error"),
            @ApiResponse(code = 500, message = "Unexpected Error"),
    })
    @POST
    @Timed
    public Transaction createTransaction(@NotNull @Valid final TransactionRequest transactionRequest)
            throws ClientErrorException {

        final Long transactionId = transactionService.createTransaction(transactionRequest);

        try {
            return transactionService.fetchById(transactionId);
        } catch (final BadRequestException e) {
            throw new InternalServerErrorException("Unexpected Error");
        }
    }
}
