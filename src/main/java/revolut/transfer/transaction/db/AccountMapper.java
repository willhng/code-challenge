package revolut.transfer.transaction.db;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import revolut.transfer.transaction.entry.Account;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountMapper implements RowMapper<Account> {
    @Override
    public Account map(final ResultSet rs, final StatementContext ctx) throws SQLException {
        return new Account(rs.getLong(Account.Fields.id), rs.getBigDecimal(Account.Fields.balance));
    }
}
