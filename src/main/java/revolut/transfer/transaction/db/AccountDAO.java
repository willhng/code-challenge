package revolut.transfer.transaction.db;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transactional;
import revolut.transfer.transaction.entry.Account;

import java.math.BigDecimal;
import java.util.Optional;

@RegisterRowMapper(AccountMapper.class)
public interface AccountDAO extends Transactional<AccountDAO> {

    @SqlUpdate("" +
            "CREATE TABLE IF NOT EXISTS `account` (\n" +
            "    `id` IDENTITY NOT NULL PRIMARY KEY,\n" +
            "    `balance` DECIMAL NOT NULL\n" +
            ");")
    void createTable();


    @SqlUpdate("INSERT INTO `account` (id, balance) VALUES (DEFAULT, :balance);")
    @GetGeneratedKeys(Account.Fields.id)
    long insert(
            @Bind(Account.Fields.balance) BigDecimal balance
    );

    @SqlUpdate("UPDATE `account` SET `balance` = :balance WHERE `id` = :id;")
    @GetGeneratedKeys(Account.Fields.id)
    long updateBalance(
            @Bind(Account.Fields.id) long id,
            @Bind(Account.Fields.balance) BigDecimal balance
    );


    @SqlQuery("SELECT * from `account` where id = :id")
    Optional<Account> findById(
            @Bind(Account.Fields.id) long id
    );
}