package revolut.transfer.transaction.db;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import revolut.transfer.transaction.entry.Transaction;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TransactionMapper implements RowMapper<Transaction> {
    @Override
    public Transaction map(final ResultSet rs, final StatementContext ctx) throws SQLException {
        return Transaction.builder()
                .id(rs.getLong(Transaction.Fields.id))
                .amount(rs.getBigDecimal(Transaction.Fields.amount))
                .senderId(rs.getLong("sender_id"))
                .receiverId(rs.getLong("receiver_id"))
                .timestamp(rs.getTimestamp(Transaction.Fields.timestamp))
                .build();
    }
}
