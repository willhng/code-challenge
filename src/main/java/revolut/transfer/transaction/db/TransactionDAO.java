package revolut.transfer.transaction.db;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transactional;
import revolut.transfer.transaction.entry.Transaction;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@RegisterRowMapper(TransactionMapper.class)
public interface TransactionDAO extends Transactional<TransactionDAO> {

    @SqlUpdate("" +
            "CREATE TABLE IF NOT EXISTS `transaction` (\n" +
            "    id IDENTITY NOT NULL PRIMARY KEY,\n" +
            "    amount DECIMAL NOT NULL,\n" +
            "    sender_id BIGINT NOT NULL,\n" +
            "    receiver_id BIGINT NOT NULL,\n" +
            "    `timestamp` TIMESTAMP(9) NOT NULL\n" +
            ");\n" +
            "CREATE INDEX IF NOT EXISTS idx_sender_id\n" +
            "ON `transaction`(sender_id);\n" +
            "CREATE INDEX IF NOT EXISTS idx_receiver_id\n" +
            "ON `transaction`(receiver_id);\n")
    void createTable();


    @SqlUpdate("INSERT INTO " +
            "`transaction` (id, amount, sender_id, receiver_id, `timestamp`) " +
            "VALUES (DEFAULT, :amount, :senderId, :receiverId, :timestamp);")
    @GetGeneratedKeys(Transaction.Fields.id)
    long insert(
            @Bind(Transaction.Fields.amount) final BigDecimal amount,
            @Bind(Transaction.Fields.senderId) final long senderId,
            @Bind(Transaction.Fields.receiverId) final long receiverId,
            @Bind(Transaction.Fields.timestamp) final Timestamp timestamp
    );


    @SqlQuery("SELECT * FROM `transaction` WHERE id = :id LIMIT 1")
    Optional<Transaction> findById(@Bind(Transaction.Fields.id) long id);


    // Index Merging
    @SqlQuery("SELECT * FROM `transaction`\n" +
            "WHERE sender_id = :accountId OR receiver_id = :accountId\n" +
            "ORDER BY id DESC\n" +
            "LIMIT :limit OFFSET :offset;")
    List<Transaction> findByAccountId(
            @Bind("accountId") final long accountId,
            @Bind("limit") final int limit,
            @Bind("offset") final long offset
    );

    @SqlUpdate("DELETE FROM `transaction`;")
    void deleteAll();
}
