package revolut.transfer.transaction.service;

import lombok.AllArgsConstructor;
import org.jdbi.v3.core.JdbiException;
import org.jdbi.v3.core.transaction.TransactionIsolationLevel;
import org.jdbi.v3.sqlobject.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import revolut.transfer.transaction.db.AccountDAO;
import revolut.transfer.transaction.db.TransactionDAO;
import revolut.transfer.transaction.entry.Account;
import revolut.transfer.transaction.entry.Transaction;
import revolut.transfer.transaction.entry.TransactionRequest;
import revolut.transfer.util.TimestampUtil;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAcceptableException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@AllArgsConstructor
public class TransactionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionService.class);

    public static final int PAGE_SIZE = 10;

    private AccountDAO accountDAO;
    private TransactionDAO transactionDAO;

    public Transaction fetchById(final long id) {
        return transactionDAO.findById(id)
                .orElseThrow(() -> new BadRequestException("No such transaction id."));
    }

    public List<Transaction> findByAccountId(final long accountId, final int pageNumber) {
        return transactionDAO.findByAccountId(accountId, PAGE_SIZE, pageNumber * PAGE_SIZE);
    }

    /**
     * In this method, the accounts' balance will be changed, and a new transaction will be create.
     * All the operations are transactional.
     * @param transactionRequest the TransactionRequest
     * @return The created transaction id
     */
    public Long createTransaction(final TransactionRequest transactionRequest) throws ClientErrorException {

        return accountDAO.inTransaction(TransactionIsolationLevel.REPEATABLE_READ, accountTx -> {
            changeBalanceForSenderAndReceiver(transactionRequest);
            return insertTransaction(transactionRequest, accountTx);
        });
    }

    // To make it testable, extract the code from the `inTransaction` lambda
    void changeBalanceForSenderAndReceiver(final TransactionRequest transactionRequest) {
        final Account sender = accountDAO.findById(transactionRequest.getSenderId())
                .orElseThrow(() -> new BadRequestException("No such account id"));
        final BigDecimal newSenderBalance = sender.getBalance()
                .subtract(transactionRequest.getAmount())
                .setScale(2, RoundingMode.HALF_UP);
        if (newSenderBalance.compareTo(BigDecimal.ZERO) < 0) {
            LOGGER.info(String.format(
                    "Balance in the account:<%d> is not enough",
                    transactionRequest.getSenderId()
            ));
            throw new NotAcceptableException("Sender's balance is not enough");
        }
        updateAccount(sender.getId(), newSenderBalance);

        final Account receiver = accountDAO.findById(transactionRequest.getReceiverId())
                .orElseThrow(() -> new BadRequestException("No such account id"));
        final BigDecimal newReceiverBalance = receiver.getBalance()
                .add(transactionRequest.getAmount())
                .setScale(2, RoundingMode.HALF_UP);
        updateAccount(receiver.getId(), newReceiverBalance);
    }

    void updateAccount(final long accountId, final BigDecimal newBalance) {
        try {
            accountDAO.updateBalance(accountId, newBalance);

        } catch (final JdbiException e) {
            LOGGER.error(String.format(
                    "Not able to update account:<%d> with balance:<%f>",
                    accountId, newBalance.doubleValue()
            ), e);
            throw new InternalServerErrorException("Unexpected internal transaction error");
        }
    }

    private Long insertTransaction(
            final TransactionRequest transactionRequest,
            final Transactional<AccountDAO> accountTx
    ) {
        return transactionDAO.inTransaction(__ ->
                insertTransactionWithoutTransactional(transactionRequest, accountTx));
    }

    // To make it testable, extract the code from the `inTransaction` lambda
    Long insertTransactionWithoutTransactional(
            final TransactionRequest transactionRequest,
            final Transactional<AccountDAO> accountTx
    ) {
        try {
            return transactionDAO.insert(
                    transactionRequest.getAmount(),
                    transactionRequest.getSenderId(),
                    transactionRequest.getReceiverId(),
                    TimestampUtil.now()
            );

        } catch (final JdbiException e) {
            accountTx.rollback();
            LOGGER.error("Unable to insert transaction, rolled back.", e);
            throw new InternalServerErrorException("Unexpected internal transaction error");
        }
    }
}
