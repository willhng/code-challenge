package revolut.transfer.transaction.entry;

import io.swagger.annotations.ApiModel;
import lombok.AccessLevel;
import lombok.Value;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@ApiModel
@Value
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Valid
@FieldNameConstants
public class Account {

    Long id;

    @Min(value = 0)
    BigDecimal balance;
}
