package revolut.transfer.transaction.entry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.sql.Timestamp;

@ApiModel
@Data
@Builder
@Valid
@FieldNameConstants
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {

    private Long id;

    @Min(value = 0)
    private BigDecimal amount;

    private Long senderId;

    private Long receiverId;

    @ApiModelProperty(dataType = "long")
    private Timestamp timestamp;
}
