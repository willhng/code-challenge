package revolut.transfer.transaction.entry;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@ApiModel
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionRequest {

    @NotNull
    @Min(value = 0)
    private BigDecimal amount;

    @NotNull
    private Long senderId;

    @NotNull
    private Long receiverId;
}
