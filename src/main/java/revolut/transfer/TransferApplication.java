package revolut.transfer;

import io.dropwizard.Application;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.jdbi3.bundles.JdbiExceptionsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.jdbi.v3.core.Jdbi;
import revolut.transfer.transaction.db.AccountDAO;
import revolut.transfer.transaction.db.TransactionDAO;
import revolut.transfer.transaction.resources.TransactionResource;
import revolut.transfer.transaction.service.TransactionService;

import java.math.BigDecimal;

public class TransferApplication extends Application<TransferConfiguration> {

    public static void main(final String[] args) throws Exception {
        new TransferApplication().run(args);
    }

    @Override
    public String getName() {
        return "transfer";
    }

    @Override
    public void run(final TransferConfiguration config, final Environment env) {
        final Jdbi jdbi = new JdbiFactory()
                .build(env, config.getDatabaseSourceFactory(), "h2");

        env.jersey().register(createTransactionResource(jdbi));
        env.healthChecks().register(getName(), new TransferHealthCheck());
    }

    @Override
    public void initialize(final Bootstrap<TransferConfiguration> bootstrap) {
        bootstrap.addBundle(new JdbiExceptionsBundle());
        bootstrap.addBundle(swaggerBundle);
        bootstrap.registerMetrics();
    }


    private TransactionResource createTransactionResource(final Jdbi jdbi) {

        final var accountDAO = jdbi.onDemand(AccountDAO.class);
        accountDAO.createTable();
        accountDAO.insert(BigDecimal.valueOf(100)); // Add account 1
        accountDAO.insert(BigDecimal.valueOf(100)); // Add account 2

        final var transactionDAO = jdbi.onDemand(TransactionDAO.class);
        transactionDAO.createTable();

        final TransactionService transactionService = new TransactionService(accountDAO, transactionDAO);

        return new TransactionResource(transactionService);
    }

    private final SwaggerBundle<TransferConfiguration> swaggerBundle = new SwaggerBundle<>() {
        @Override
        protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(TransferConfiguration configuration) {
            return configuration.swaggerBundleConfiguration;
        }
    };
}
