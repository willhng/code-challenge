package revolut.transfer.util;

import java.sql.Timestamp;
import java.time.Instant;

public final class TimestampUtil {

    // Discard the nano seconds, because the database doesn't support
    public static Timestamp now() {
        var timestamp = Timestamp.from(Instant.now());
        timestamp.setNanos(0);
        return timestamp;
    }
}
