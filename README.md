# Revolut Code Challenge

## How to start the application

1. Run `mvn clean package` to build your application
2. Start application with `java -jar target/transfer-1.0-SNAPSHOT.jar server config.yml`
3. To check the information of the API http://localhost:8080/swagger

**Note**: when you run the `mvn clean package`, some error log will show up. No worries, that's intended error log.

Or, if you want to run the application with **IntelliJ**, please run the `TransferApplication` class,
you will see that the arguments are missed, please edit the [Run/Debug Configurations](https://www.jetbrains.com/help/idea/creating-and-editing-run-debug-configurations.html), and copy/paste the `server config.yml` to the *Program arguments*.

## Overview

This project is using the Dropwizard, with Jersey and JDBI. The in-memory datastore is H2.

And it's using Swagger for the API doc.

**Q**: Why not use Kotlin? 

**A**: I love Kotlin, but I didn't find many people develop Dropwizard with Kotlin. 
To avoiding potential problems and saving the time, I chose Java 11.
## Swagger & Try it out

This project is using Swagger, you will be able to find all the API endpoints' and models' information in http://localhost:8080/swagger#/default, and you can send requests easily by clicking the *Try it out* button.

For you easy to create transaction, there are 2 accounts are prepared for you, the IDs are 1 and 2.

## Health Check

To see your applications health enter URL http://localhost:8081/healthcheck

This functional is a built-in feature from Dropwizard, to see more metrics features, please enter URL http://localhost:8081

## System Design
In this project, there are two models involved: *Account* and *Transaction*. The *Account* is storing the account's balance, and the *Transaction* is recording of transactions, in one transaction, the money is always transferred between two accounts.

For transfer the money, the most important thing is the data consistency. The cases below should be avoided:

1. The transfer amount was deducted in the sender's balance, but not increased in the receiver's balance.
2. The transfer amount was increased in the receiver's balance, but not deducted in the sender's balance.
3. The accounts' balances are changed in the database, but the transaction record was not created.
4. The transaction record was created, but the accounts' balances are not changed, or rolled back.
5. The sender should not be able to send the amount of money that he/she doesn't have. 
    
    e.g. The sender's balance is $100.00, he/she instantly send $100.00 twice. One of the transaction should failed.

For the **points 1-4**, the simplest implementation within one simple project is to make all the account and transaction operations transactional / atomical. 

However, the operations for account and transaction are in two different domains, to make those operations in one database transaction, it will need to call the *AccountDAO* and *TransactionDAO* within one single service-level method call. (check the code in *TransactionService::createTransaction()*)

This design is not following the *Domain-Driven Design*, but the aim is to keep it simple and to the point.

And for the **point 5**, hereby I am using the *Repeatable Read* as the transaction isolation level.